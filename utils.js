const fetch = require('node-fetch');
const _ = require('lodash');

const Utils = {
  getAllResultsUnsorted: async(url) => {
    const response = await fetch(url);
    if (response.status !== 200) return [];

    const firstResponse = await response.json();
    const totalCount = firstResponse.count;
    const resultsPerPage = firstResponse.results.length;
    const totalPages = Math.ceil(totalCount/resultsPerPage);
  
    // Get allPromises ready, send them all at once
    const allPromises = [];
    for (let i=2;i<=totalPages;i++) {
      allPromises.push(fetch(`${url}?page=${i}`));
    }

    const allResponses = await Promise.all(allPromises);
    for(let i=0;i<allResponses.length;i++) {
      allResponses[i] = await allResponses[i].json();
    }
    allResponses.unshift(firstResponse);
  
    let allResultsUnsorted = [];
    for(let i=0;i<allResponses.length;i++) {
      allResultsUnsorted = [
        ...allResultsUnsorted,
        ...allResponses[i].results
      ];
    }

    return allResultsUnsorted;
  }

};
  
module.exports = Utils;