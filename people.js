const fetch = require('node-fetch');
const _ = require('lodash');
const Utils = require('./utils');

const getAllPeople = async(req, res) => {
  const sortBy = _.get(req, 'query.sortBy', '');
  const peopleUrl = 'https://swapi.dev/api/people';

  const allResultsUnsorted = await Utils.getAllResultsUnsorted(peopleUrl);

  // Sort Results
  if (sortBy === 'name') {
    const sorted = allResultsUnsorted.sort((a, b) => (a.name > b.name) ? 1 : -1);
    res.send(sorted);
  } else if (sortBy === 'height') {
    const sorted = allResultsUnsorted.sort((a, b) => {
      const aNum = isNaN(a.height) ? 0 : parseInt(a.height);
      const bNum = isNaN(b.height) ? 0 : parseInt(b.height);
      return (aNum > bNum) ? 1 : -1;
    });
    res.send(sorted);
  } else if (sortBy === 'mass') {
    const sorted = allResultsUnsorted.sort((a, b) => {
      const aNum = isNaN(a.mass.replace(',', '')) ? 0 : parseInt(a.mass.replace(',', ''));
      const bNum = isNaN(b.mass.replace(',', '')) ? 0 : parseInt(b.mass.replace(',', ''));
      return (aNum > bNum) ? 1 : -1;
    });
    res.send(sorted);
  } else {
    res.send(allResultsUnsorted);
  }
  
};
  
module.exports = getAllPeople;