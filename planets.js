const fetch = require('node-fetch');
const _ = require('lodash');
const Utils = require('./utils');

const getAllPlanets = async(req, res) => {
  const planetsUrl = 'https://swapi.dev/api/planets';

  const allResultsUnsorted = await Utils.getAllResultsUnsorted(planetsUrl);

  const allResidentsPromises = [];
  for(let i=0;i<allResultsUnsorted.length;i++) {
    for(let r=0;r<allResultsUnsorted[i].residents.length;r++) {
      allResidentsPromises.push(fetch(allResultsUnsorted[i].residents[r]));
    }
  }

  const allResidentsResponses = await Promise.all(allResidentsPromises);
  for(let i=0;i<allResidentsResponses.length;i++) {
    allResidentsResponses[i] = await allResidentsResponses[i].json();
  }

  // Update all residents with name
  for(let i=0;i<allResultsUnsorted.length;i++) {
    for(let r=0;r<allResultsUnsorted[i].residents.length;r++) {
      const match = allResidentsResponses.find(o => o.url === allResultsUnsorted[i].residents[r]);
      allResultsUnsorted[i].residents[r] = match.name;
    }
  }

  res.send(allResultsUnsorted);
};


module.exports = getAllPlanets;
