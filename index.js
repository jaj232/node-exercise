const express = require('express');
const app = express();

const getAllPlanets = require('./planets');
const getAllPeople = require('./people');

app.use(express.json());

app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

// Routes
app.get('/people', (req, res) => getAllPeople(req, res));
app.get('/planets', (req, res) => getAllPlanets(req, res));

const port = 3000;
app.listen(port, () => console.log(`app listening at http://localhost:${port}`));
